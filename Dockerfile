ARG REPO_ARCH

FROM $REPO_ARCH/openjdk:17-oracle

ARG UID=991
ARG GID=991

RUN \
  groupadd --gid "${GID}" app && \
  adduser -u "${UID}" -g app -d /opt/app app

USER app
WORKDIR /opt/app

VOLUME /mnt/app
CMD ["/usr/bin/java", "-Xms2G", "-Xmx2G", "-jar", "/opt/app/server.jar"]
